# ufsprinkle-sevak

## Helper Sprinkle for UserFrosting

Install the following UserFrosting Sprinkles as submodules in you
app/sprinkles directory

* git submodule add git@github.com:ssnukala/ufsprinkle-datatables.git Datatables
* git submodule add git@github.com:ssnukala/ufsprinkle-snutilities SnUtilities
* git submodule add git@github.com:ssnukala/ufsprinkle-autoforms AutoForms
* git submodule add git@github.com:ssnukala/ufsprinkle-sevak Sevak


### Go to 

http://yoursite.vhost/venues/dashboard

You will see the datatables with 